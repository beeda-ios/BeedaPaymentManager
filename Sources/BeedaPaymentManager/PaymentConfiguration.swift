import UIKit
import SwiftKeychainWrapper
import RKAPIService
import BeedaUtilities
@_spi(BPM) import BeedaPaymentBase

public typealias BeedaPaymentManager = PaymentConfiguration
public typealias BeedaPaymentDelegate = PaymentCardDelegate

public typealias BeedaCard = Card

public class PaymentConfiguration {
    private let alertViewTag: Int = 25378
    
    var networkService: RKAPIService = .shared
    var urlScheme: String = "https"
    var baseURL: String = ""
    var pathPrefix: String?
    var beedaLogo: UIImage?
    var appDelegate: UIApplicationDelegate?
    var returnDelegate: (any BeedaPaymentDelegate)?
    var window: UIWindow?
    
    var transactionAmount: Double?
    var transactionID: String?
    var transactionCurrencySymbol: String?
    
    public var  initialView = SelectPaymnetMethodViewController(bundle: .module)
    
    public func setup(networkService: RKAPIService, scheme: String = "https", baseURL: String, window: UIWindow?, returnTo delegate: (any BeedaPaymentDelegate)?) {
        self.networkService = networkService
        self.urlScheme = scheme
        self.baseURL = baseURL
        self.window = window
        self.returnDelegate = delegate
        
        let config = PaymentConfigurationBase(urlScheme: scheme, baseURL: baseURL, pathPrefix: nil, portNo: nil, networkService: networkService, window: window)
        
        CONFIG = config
        
        manager = self
    }
    
    public func setup(networkService: RKAPIService, scheme: String = "https", baseURL: String, transactionAmount: Double, currencySymbol: String, transactionID: String? = nil, window: UIWindow?, returnTo delegate: (any BeedaPaymentDelegate)?) {
        self.networkService = networkService
        self.urlScheme = scheme
        self.baseURL = baseURL
        self.window = window
        self.returnDelegate = delegate
        
        self.transactionAmount = transactionAmount
        self.transactionID = transactionID
        self.transactionCurrencySymbol = currencySymbol
        
        let config = PaymentConfigurationBase(urlScheme: scheme, baseURL: baseURL, pathPrefix: nil, portNo: nil, networkService: networkService, window: window)
        
        CONFIG = config
        
        manager = self
    }
    
    public init() {}
}

internal var manager = BeedaPaymentManager()
