//
//  AddCardView.swift
//  
//
//  Created by Rakibur Khan on 16/7/23.
//

import UIKit
import StripePayments
@_spi(BPM) import BeedaPaymentBase

class AddCardView: UIView {
    @IBOutlet private weak var background: UIView!
    @IBOutlet private weak var safeAreaPaddingView: UIView!
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var scrollViewHeight: NSLayoutConstraint!
    
    @IBOutlet private weak var addNewCardTitle: UILabel!
    
    @IBOutlet private weak var cardNumberBackground: UIView!
    @IBOutlet private weak var cardNumberField: UITextField!
    @IBOutlet private weak var cardTypeImage: UIImageView!
    
    @IBOutlet private weak var monthYearBackground: UIView!
    @IBOutlet private weak var monthYearField: UITextField!
    
    @IBOutlet private weak var cvcFieldBackground: UIView!
    @IBOutlet private weak var cvcField: UITextField!
    
    @IBOutlet private weak var billingAddressTitle: UILabel!
    
    @IBOutlet private weak var fullNameBackground: UIView!
    @IBOutlet private weak var fullNameField: UITextField!
    
    @IBOutlet private weak var emailBackground: UIView!
    @IBOutlet private weak var emailField: UITextField!
    
    @IBOutlet private weak var stateBackground: UIView!
    @IBOutlet private weak var stateField: UITextField!
    
    @IBOutlet private weak var zipcodeBackground: UIView!
    @IBOutlet private weak var zipcodeField: UITextField!
    
    @IBOutlet private weak var addressBackground: UIView!
    @IBOutlet private weak var addressField: UITextField!
    
    @IBOutlet private weak var phoneBackground: UIView!
    @IBOutlet private weak var phoneField: UITextField!
    
    @IBOutlet private weak var addCardButton: UIButton!
    
    private var cardBrand: STPCardBrand?
    
    private var cardLength: Int = 13
    private var cvcLength: Int = 3
    
    var cardDetails: ((CardRequestModel)-> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        loadViewFromNib(bundle: .module)
        
        setView()
    }
    
    func setupScrollView() {
        scrollViewHeight.constant = scrollView.contentSize.height
    }
    
    @IBAction private func textFieldTextChanges(_ textField: UITextField) {
        if textField == cardNumberField {
            validateCardBrand(cardNumber: textField.text)
        } else  if textField == monthYearField {
            if let text = textField.text {
                if text.count == 5 {
                    _ = textField.delegate?.textFieldShouldReturn?(textField)
                }
            }
        } else if textField == cvcField {
            if let text = textField.text {
                textField.text = STPCardValidator.sanitizedNumericString(for: text)
                
                if text.count == cvcLength {
                    _ = textField.delegate?.textFieldShouldReturn?(textField)
                }
            }
        }
    }
    
    @IBAction private func addCardAction(_ sender: UIButton) {
        guard var number = cardNumberField.text else {
            return
        }
        
        number = STPCardValidator.sanitizedNumericString(for: number)
        
        guard let monthYear = monthYearField.text, isExpiryValid(monthYear) else {
            return
        }
        
        guard var cvc = cvcField.text else {
            return
        }
        
        cvc = STPCardValidator.sanitizedNumericString(for: cvc)
        
        guard let name = fullNameField.text else {
            return
        }
        
        let components = monthYear.split(separator: "/")
        let month: String = String(components.first ?? "")
        let year: String = String(components.last ?? "")
        
        let region = stateField.text
        
        var zipCode: String?
        if let text = zipcodeField.text {
            zipCode = STPCardValidator.sanitizedPostalString(for: text)
        }
        
        let card: CardRequestModel = .init(cardNo: number, name: name, month: month, year: year, cvv: cvc, country: "", state: "", zip: zipCode ?? "", phone: "", address: "", email: "")
        
        cardDetails?(card)
    }
 }

extension AddCardView {
    private func setView() {
        switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                background.roundedCorner(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 20)
                safeAreaPaddingView.isHidden = false
                
            default:
                background.roundedCorner(radius: 20)
                safeAreaPaddingView.isHidden = true
        }
        
        addNewCardTitle.text = NSLocalizedString("Add new card", comment: "Text: Add new card")
        addNewCardTitle.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 16)
        addNewCardTitle.textColor = .color2E3A59
        
        cardNumberField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        cardNumberField.keyboardType = .numberPad
        cardNumberField.textContentType = .creditCardNumber
        cardNumberField.returnKeyType = .next
        cardNumberField.delegate = self
        cardNumberField.placeholder = NSLocalizedString("Card Number*", comment: "TextField: Card Number*")
        cardNumberBackground.roundedCorner(radius: 10)
        cardNumberBackground.setBorder(color: .colorDFE5FF)
        
        cardTypeImage.image = nil
        
        monthYearField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        monthYearField.keyboardType = .numberPad
        monthYearField.returnKeyType = .next
        monthYearField.delegate = self
        monthYearField.placeholder = NSLocalizedString("MM / YY*", comment: "TextField: MM / YY*")
        monthYearBackground.roundedCorner(radius: 10)
        monthYearBackground.setBorder(color: .colorDFE5FF)
        
        cvcField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        cvcField.keyboardType = .numberPad
        cvcField.returnKeyType = .next
        cvcField.delegate = self
        cvcField.placeholder = NSLocalizedString("CVC*", comment: "TextField: CVC*")
        cvcFieldBackground.roundedCorner(radius: 10)
        cvcFieldBackground.setBorder(color: .colorDFE5FF)
        
        billingAddressTitle.text = NSLocalizedString("Billing Address", comment: "Text: Billing Address")
        billingAddressTitle.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 16)
        billingAddressTitle.textColor = .color2E3A59
        
        fullNameField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        fullNameField.textContentType = .name
        fullNameField.keyboardType = .alphabet
        fullNameField.autocapitalizationType = .allCharacters
        fullNameField.returnKeyType = .next
        fullNameField.delegate = self
        fullNameField.placeholder = NSLocalizedString("Full Name*", comment: "TextField: Full Name*")
        fullNameBackground.roundedCorner(radius: 10)
        fullNameBackground.setBorder(color: .colorDFE5FF)
        
        emailField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        emailField.textContentType = .emailAddress
        emailField.keyboardType = .emailAddress
        emailField.returnKeyType = .continue
        emailField.delegate = self
        emailField.placeholder = NSLocalizedString("Email Address*", comment: "TextField: Email Address*")
        emailBackground.roundedCorner(radius: 10)
        emailBackground.setBorder(color: .colorDFE5FF)
        
        stateField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        stateField.textContentType = .addressState
        stateField.keyboardType = .alphabet
        stateField.returnKeyType = .next
        stateField.delegate = self
        stateField.placeholder = NSLocalizedString("State/Province*", comment: "TextField: State/Province*")
        stateBackground.roundedCorner(radius: 10)
        stateBackground.setBorder(color: .colorDFE5FF)
        
        zipcodeField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        zipcodeField.textContentType = .postalCode
        zipcodeField.keyboardType = .alphabet
        zipcodeField.returnKeyType = .next
        zipcodeField.delegate = self
        zipcodeField.placeholder = NSLocalizedString("Zip Code*", comment: "TextField: Zip Code*")
        zipcodeBackground.roundedCorner(radius: 10)
        zipcodeBackground.setBorder(color: .colorDFE5FF)
        
        addressField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        addressField.textContentType = .fullStreetAddress
        addressField.keyboardType = .alphabet
        addressField.returnKeyType = .next
        addressField.autocapitalizationType = .words
        addressField.delegate = self
        addressField.placeholder = NSLocalizedString("Address*", comment: "TextField: Address*")
        addressBackground.roundedCorner(radius: 10)
        addressBackground.setBorder(color: .colorDFE5FF)
        
        phoneField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        phoneField.textContentType = .telephoneNumber
        phoneField.keyboardType = .phonePad
        phoneField.returnKeyType = .continue
        phoneField.delegate = self
        phoneField.placeholder = NSLocalizedString("Phone number*", comment: "TextField: Phone number*")
        phoneBackground.roundedCorner(radius: 10)
        phoneBackground.setBorder(color: .colorDFE5FF)
    
        addCardButton.customizeButton(title: NSLocalizedString("Add card", comment: "Button: Add card"), font: .poppins(forTextStyle: .callout, type: .medium, fontSize: 18), textColor: .systemBackground, backgroundColor: .color163BDE, cornerRadius: 10, edgeInsets: .init(top: 10, leading: 110, bottom: 10, trailing: 110))
    }
}

extension AddCardView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)
        
        if textField == cardNumberField {
            if newString.count <= cardLength {
                return true
            } else {
                _ = textField.delegate?.textFieldShouldReturn?(textField)
                
                return false
            }
        } else if textField == monthYearField {
            if range.length > 0 {
                return true
            }
            
            if string == "" {
                return false
            }
            
            if range.location > 4 {
                return false
            }
            
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            //Verify entered text is a numeric value
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            //Put / after 2 digit
            if range.location == 2 {
                originalText?.append("/")
                textField.text = originalText
            }
            return true
            
        } else if textField == cvcField {
            if newString.count <= cvcLength {
                return true
            } else {
                _ = textField.delegate?.textFieldShouldReturn?(textField)
                
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardNumberField {
            monthYearField.becomeFirstResponder()
        } else if textField == monthYearField {
            cvcField.becomeFirstResponder()
        } else if textField == cvcField {
            fullNameField.becomeFirstResponder()
        } else if textField == fullNameField {
            emailField.becomeFirstResponder()
        } else if textField == stateField {
            zipcodeField.becomeFirstResponder()
        } else if textField == zipcodeField {
            addressField.becomeFirstResponder()
        } else if textField == addressField {
            phoneField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}

extension AddCardView {
    func validateCardBrand(cardNumber: String?) {
        guard let cardNumber: String = cardNumber else {
            cardTypeImage.image = nil
            
            return
        }
        
        let brand = STPCardValidator.brand(forNumber: cardNumber)
        cardBrand = brand
        cvcLength = STPCardValidator.maxLength(for: brand)
        cardLength = STPCardValidator.maxLength(for: brand)
        
        cardNumberField.text = STPCardValidator.sanitizedNumericString(for: cardNumber)
        
        if cardNumber.count == cardLength {
            _ = cardNumberField.delegate?.textFieldShouldReturn?(cardNumberField)
        }
        
        switch brand {
            case .visa:
                cardTypeImage.image = UIImage(named: "visa")
            case .amex:
                cardTypeImage.image = UIImage(named: "express")
            case .mastercard:
                cardTypeImage.image = UIImage(named: "master")
            case .discover:
                cardTypeImage.image = UIImage(named: "discover")
            
            default:
                cardTypeImage.image = nil
        }
    }
    
    func isExpiryValid(_ value: String) -> Bool {
        if value.count >= 5 {
            
            let components = value.split(separator: "/")
            let month = Int(components.first!) ?? 0
            let year = Int(components.last!) ?? 0
            let date = Date()
            let calendar = Calendar.current
            let currentYear = calendar.component(.year, from: date)
            let currentMonth = calendar.component(.month, from: date)
            let yearLastTwo = currentYear % 100
            
            if month > 12 {
                return false
            }
            else if year == yearLastTwo {
                if month >= currentMonth {
                    return true
                }
                else {
                    return false
                }
            }
            else if year >= yearLastTwo {
                return true
            }
            else {
                return false
            }
        }
        return false
    }
}
