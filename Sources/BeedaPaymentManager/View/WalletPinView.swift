// 
// 
//
// Created by Rakibur Khan on 27/7/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import UIKit

class WalletPinView: UIView {
    @IBOutlet private weak var blurView: UIView!
    @IBOutlet private weak var background: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var pinBackground: UIView!
    @IBOutlet private weak var pinField: UITextField! {
        didSet {
            pinField.setSecureTextToggleToRight(.systemGray)
        }
    }
    
    @IBOutlet private weak var submitButton: UIButton!
    
    var pin: ((String)-> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    private func commonInit() {
        loadViewFromNib(bundle: .module)
        
        setView()
        setupTapGuesture()
    }
    
    func setView() {
        background.roundedCorner(radius: 10)
        titleLabel.text = NSLocalizedString("Enter PIN", comment: "Text: Enter PIN")
        titleLabel.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 22)
        titleLabel.textColor = .color2E3A59
        
        pinBackground.roundedCorner(radius: 10)
        pinBackground.setBorder(color: .colorDFE5FF, background: .systemBackground)
        
        pinField.placeholder = NSLocalizedString("Enter wallet PIN", comment: "Text: Enter wallet PIN")
        pinField.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        pinField.textColor = .color2E3A59
        
        pinField.keyboardType = .numberPad
        pinField.textContentType = .password
        
        submitButton.customizeButton(title: NSLocalizedString("Confirm", comment: "ButtonText: Confirm"), font: .poppins(forTextStyle: .callout, type: .medium, fontSize: 16), textColor: .systemBackground, backgroundColor: .color163BDE, edgeInsets: .init(top: 3, leading: 15, bottom: 3, trailing: 15))
        submitButton.roundedCorner(radius: 6)
    }
}

extension WalletPinView {
    private func setupTapGuesture() {
        blurView.isUserInteractionEnabled = true
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(blurViewTapped))
        blurView.addGestureRecognizer(tapGuesture)
    }
    
    @objc private func blurViewTapped() {
            dismiss(animated: true)
    }
    
    private func dismiss(animated: Bool) {
        if animated {
            UIView.animate(withDuration: TimeInterval(0.7)) {
                self.removeFromSuperview()
            }
        } else {
            self.removeFromSuperview()
        }
    }
    
    @IBAction private func submit(_ sender: UIButton) {
        if let pin = pinField.text {
            self.pin?(pin)
            
            dismiss(animated: true)
        }
    }
}
