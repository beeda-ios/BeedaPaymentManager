//
//  PaymentMethodTableViewCell.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit
@_spi(BPM) import BeedaPaymentBase

class PaymentMethodTableViewCell: UITableViewCell {
    @IBOutlet private weak var background: UIView!
    @IBOutlet private weak var selectionImage: UIImageView!
    @IBOutlet private weak var paymentIcon: UIImageView!
    @IBOutlet private weak var paymentName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        background.roundedCorner(radius: 10)
        background.setBorder(color: .colorDFE5FF)
    }
    
    private func setupView() {
        paymentName.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 14)
        paymentName.textColor = .label
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        selectionImage.tintColor = .colorC4C4C4
        
        if selected {
            var image: UIImage? = UIImage(systemName: "record.circle")
            if #available(iOS 15.0, *) {
                let config = UIImage.SymbolConfiguration(paletteColors: [.systemGreen, .color163BDE])
                image = UIImage(systemName: "record.circle", withConfiguration: config)
            }
            
            selectionImage.image = image
        } else {
            let image = UIImage(systemName: "circle")
            
            selectionImage.image = image
        }
    }
}

extension PaymentMethodTableViewCell {
    func setupCell(_ payment: PaymentType) {
        paymentName.text = payment.listTitle
        paymentIcon.image = payment.icon
    }
}
