//
//  AddCardButtonView.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit

class AddCardButtonView: UITableViewHeaderFooterView {
    
    @IBOutlet private weak var addCardLabel: UILabel!
    
    private var addCard: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    @IBAction private func addCardAction(_ sender: UIButton) {
        addCard?()
    }
}

extension AddCardButtonView {
    private func setView() {
        addCardLabel.text = NSLocalizedString("Add new card", comment: "Button: Add new card")
        addCardLabel.font = .poppins(forTextStyle: .callout, type: .medium, fontSize: 15)
        addCardLabel.textColor = .color163BDE
    }
    
    func setupView(action: @escaping ()-> Void) {
        addCard = action
    }
}
