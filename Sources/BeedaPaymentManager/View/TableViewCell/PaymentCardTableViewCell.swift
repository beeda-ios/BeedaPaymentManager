//
//  PaymentCardTableViewCell.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit
import StripePayments
@_spi(BPM) import BeedaPaymentBase

class PaymentCardTableViewCell: UITableViewCell {
    @IBOutlet private weak var background: UIView!
    @IBOutlet private weak var selectionImage: UIImageView!
    @IBOutlet private weak var cardIcon: UIImageView!
    @IBOutlet private weak var cardNumber: UILabel!
    @IBOutlet private weak var deleteIcon: UIImageView!
    
    private var delegate: SelectionDelegate?
    private var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        background.roundedCorner(radius: 10)
        background.setBorder(color: .colorDFE5FF)
    }
    
    private func setupView() {
        cardNumber.font = .poppins(forTextStyle: .body, type: .regular, fontSize: 15)
        cardNumber.textColor = .label
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        selectionImage.tintColor = .colorC4C4C4
        
        if selected {
            var image: UIImage? = UIImage(systemName: "record.circle")
            if #available(iOS 15.0, *) {
                let config = UIImage.SymbolConfiguration(paletteColors: [.systemGreen, .color163BDE])
                image = UIImage(systemName: "record.circle", withConfiguration: config)
            }
            
            selectionImage.image = image
        } else {
            selectionImage.image = UIImage(systemName: "circle")
        }
    }
    
    @IBAction private func deleteCard(_ sender: UIButton) {
        guard let indexPath = indexPath else {return}
        
        delegate?.selectedIndexPath(at: indexPath, sender: sender)
    }
}

extension PaymentCardTableViewCell {
    func setupCell(_ card: Card, indexPath: IndexPath, delegate: SelectionDelegate) {
        cardNumber.text = card.cardNo
        cardIcon.load(url: card.icon)
        self.delegate = delegate
        self.indexPath = indexPath
        _ = STPCardValidator.fragmentLength(for: .visa)
    }
}
