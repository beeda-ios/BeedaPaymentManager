//
//  SelectionDelegate.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit

protocol SelectionDelegate {
    func selectedIndexPath(at indexPath: IndexPath, sender: UIButton?)
    
    func selectedIndex(at index: Int)
}

extension SelectionDelegate {
    func selectedIndexPath(at indexPath: IndexPath, sender: UIButton?){}
    
    func selectedIndex(at index: Int){}
}
