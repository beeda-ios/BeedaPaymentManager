//
//  CardListViewController.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit
import Combine
import BeedaAlertBase
@_spi(BPM) import BeedaPaymentBase

class CardListViewController: UIViewController {
    private typealias DataSource = UITableViewDiffableDataSource<Section, Card>
    
    @IBOutlet private weak var addCardContainer: UIView!
    @IBOutlet private weak var cardListContainer: UIView!
    
    @IBOutlet private weak var yourSavedCardLabel: UILabel!
    @IBOutlet private weak var cardList: UITableView!
    @IBOutlet private weak var payButton: UIButton!
    
    var viewModel: PaymentCardListViewModel = .init()
    private var dataSource: DataSource?
    private var cancellable = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 14.0, *) {
            navigationItem.backButtonDisplayMode = .minimal
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        setupView()
        setupListView()
        setupBinding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Task {
            await viewModel.fetchAllCards()
        }
    }
    
    @IBAction private func continuePayment(_ sender: UIButton) {
        if let selectedCard = viewModel.selectedCard {
            manager.returnDelegate?.cardSelected(selectedCard)
        }
    }
}

//MARK: - View Customization
extension CardListViewController {
    private func setupView() {
        //MARK: - Add Card View
        let view = AddCardView()
        
        addCardContainer.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(greaterThanOrEqualTo: addCardContainer.safeAreaLayoutGuide.topAnchor),
            view.leadingAnchor.constraint(equalTo: addCardContainer.safeAreaLayoutGuide.leadingAnchor),
            addCardContainer.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            addCardContainer.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        view.cardDetails = { [weak self] card in
            self?.viewModel.addCard(card)
        }
        
        //MARK: - Card list View
        yourSavedCardLabel.text = NSLocalizedString("Your saved cards", comment: "Text: Your saved cards")
        yourSavedCardLabel.font = .poppins(forTextStyle: .headline, type: .regular, fontSize: 12)
        yourSavedCardLabel.textColor = .color979797
        
        payButton.customizeButton(title: NSLocalizedString("Make Payment", comment: "Button: Make Payment"), font: .poppins(forTextStyle: .callout, type: .medium, fontSize: 18), textColor: .systemBackground, backgroundColor: .color163BDE, cornerRadius: 10, edgeInsets: .init(top: 10, leading: 80, bottom: 10, trailing: 80))
    }
}

extension CardListViewController {
    private func setupListView() {
        cardList.delegate = self
        cardList.separatorStyle = .none
        cardList.bounces = false
        cardList.register(cell: PaymentCardTableViewCell.self, bundle: .module)
        cardList.register(headerFooter: AddCardButtonView.self, bundle: .module)
        
        setupDataSource()
    }
    
    private func setupDataSource() {
        dataSource = DataSource(tableView: cardList) { tableView, indexPath, card in
            
            guard let cell = tableView.dequeueReusableCell(cell: PaymentCardTableViewCell.self, indexPath: indexPath) else {fatalError("Can't dequeue cell")}
            
            cell.selectionStyle = .none
            
            cell.setupCell(card, indexPath: indexPath, delegate: self)
            
            return cell
        }
    }
    
    private func updateData(_ data: [Card]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Card>()
        snapshot.appendSections([.main])
        snapshot.appendItems(data, toSection: .main)
        
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}

extension CardListViewController {
    private func setupBinding() {
        viewModel.$cards
            .receive(on: DispatchQueue.main)
            .sink { [weak self] cards in
                if cards.isEmpty {
                    self?.addCardContainer.isHidden = false
                    self?.cardListContainer.isHidden = true
                } else {
                    self?.addCardContainer.isHidden = true
                    self?.cardListContainer.isHidden = false
                    self?.updateData(cards)
                }
            }
            .store(in: &cancellable)
        
        viewModel.$selectedCard
            .receive(on: DispatchQueue.main)
            .sink { [weak self] card in
                if card == nil {
                    self?.payButton.isEnabled = false
                } else {
                    self?.payButton.isEnabled = true
                }
            }
            .store(in: &cancellable)
    }
}

extension CardListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let card = dataSource?.itemIdentifier(for: indexPath) else {return}
        
        viewModel.updateSelectedCard(card)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(view: AddCardButtonView.self)
        footer?.setupView(action: addCardAction)
        return footer
    }
    
    private func addCardAction() {
        let viewController = AddCardViewController(bundle: .module)
        viewController.viewModel = viewModel
        
        present(viewController, animated: true)
    }
}

extension CardListViewController: SelectionDelegate {
    func selectedIndexPath(at indexPath: IndexPath, sender: UIButton?) {
        guard let card = dataSource?.itemIdentifier(for: indexPath) else {return}
        
        let defaultAction: CustomAlertAction = .init(title: NSLocalizedString("Remove", comment: "Button: Remove"), style: .destructive) {
            self.viewModel.removeCard(card.id)
        }
        let cancelAction: CustomAlertAction = .init(title: NSLocalizedString("Cancel", comment: "Button: Cancel"), style: .cancel)
        
        utility.showAlert(title: NSLocalizedString("Are you sure to remove this card?", comment: "Text: Are you sure to remove this card?"), type: .warning, defaultAction: defaultAction, cancelAction: cancelAction)
    }
}

fileprivate enum Section {
    case main
}
