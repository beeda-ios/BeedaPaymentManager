//
//  AddCardViewController.swift
//  
//
//  Created by Rakibur Khan on 16/7/23.
//

import UIKit
import Combine
@_spi(BPM) import BeedaPaymentBase

class AddCardViewController: UIViewController {

    private var addCardView: AddCardView?
    
    var viewModel: PaymentCardListViewModel?
    
    private var cancellable = Set<AnyCancellable>()
    
    override func loadView() {
        super.loadView()
        
        setupBinding()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 14.0, *) {
            navigationItem.backButtonDisplayMode = .minimal
        } else {
            // Fallback on earlier versions
        }
        
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        addCardView?.setupScrollView()
    }

    private func setupView() {
        let view = AddCardView()

        self.view.addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false

        switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                NSLayoutConstraint.activate([
                    view.topAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.topAnchor),
                    view.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                    self.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                    self.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
                ])
                
            default:
                NSLayoutConstraint.activate([
                    view.topAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
                    view.leadingAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
                    self.view.safeAreaLayoutGuide.bottomAnchor.constraint(greaterThanOrEqualTo: view.bottomAnchor, constant: 10),
                    self.view.safeAreaLayoutGuide.trailingAnchor.constraint(greaterThanOrEqualTo: view.trailingAnchor, constant: 10),
                    view.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
                    view.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor)
                ])
        }
        
        addCardView = view
        
        view.cardDetails = { [weak self] card in
            self?.viewModel?.addCard(card)
        }
    }
}

extension AddCardViewController {
    func setupBinding() {
        viewModel?.$listUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] operationDone in
                if let complete = operationDone, complete {
                    self?.dismiss(animated: true) {
                        self?.viewModel?.resetListStatus()
                    }
                }
            }
            .store(in: &cancellable)
    }
}

