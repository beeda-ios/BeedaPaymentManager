//
//  SelectPaymentMethodViewController.swift
//  
//
//  Created by Rakibur Khan on 21/7/23.
//

import UIKit
import Combine
@_spi(BPM) import BeedaPaymentBase

public class SelectPaymnetMethodViewController: UIViewController {
    private typealias DataSource = UITableViewDiffableDataSource<Section, PaymentType>
    
    @IBOutlet private weak var transactionSummaryView: UIView!
    @IBOutlet private weak var transactionBackground: UIView!
    @IBOutlet private weak var transactionAmountBackground: UIView!
    @IBOutlet private weak var transactionAmount: UILabel!
    @IBOutlet private weak var transactionCurrency: UILabel!
    @IBOutlet private weak var transactionIDLabel: UILabel!
    
    @IBOutlet private weak var paymentMethodListBanner: UIView!
    
    @IBOutlet private weak var paymentList: UITableView!
    @IBOutlet private weak var payButton: UIButton!
    
    var viewModel: SelectPaymnetMethodViewModel = .init()
    
    private var dataSource: DataSource?
    private var cancellable = Set<AnyCancellable>()
    
    private let paymentMethods: [PaymentType] = [.wallet, .card]
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Payment Options", comment: "NavigationTitle: Payment Options")
        if #available(iOS 14.0, *) {
            navigationItem.backButtonDisplayMode = .minimal
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
        setupView()
        setupListView()
        setupBinding()
    }
    
    @IBAction private func continuePayment(_ sender: UIButton) {
        switch viewModel.selectedMethod {
            case .wallet:
                showWalletPinView()
                
            case .card:
                let coordinator = PaymentCoordinator(navigationController: navigationController)
                coordinator.navigateToCardList()
                
            default:
                break
        }
    }
    
    private func showWalletPinView() {
        let pinView = WalletPinView()
        pinView.pin = walletPin
        
        view.addSubview(pinView)
        
        pinView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    private func walletPin(_ pin: String) {
        if !pin.isEmpty {
            manager.returnDelegate?.walletSelected(pin)
        }
    }
}

//MARK: - View Customization
extension SelectPaymnetMethodViewController {
    private func setupView() {
        setupTransactionView(amount: manager.transactionAmount, currencySymbol: manager.transactionCurrencySymbol, transactionID: manager.transactionID)
        
        payButton.customizeButton(title: NSLocalizedString("Continue to Pay", comment: "Button: Continue to Pay"), font: .poppins(forTextStyle: .callout, type: .medium, fontSize: 18), textColor: .systemBackground, backgroundColor: .color163BDE, cornerRadius: 10, edgeInsets: .init(top: 10, leading: 80, bottom: 10, trailing: 80))
        updatePayButton(title: NSLocalizedString("Continue to Pay", comment: "Button: Continue to Pay"))
    }
    
    private func updatePayButton(title: String) {
        payButton.customizeButton(title: title, font: .poppins(forTextStyle: .callout, type: .medium, fontSize: 18), textColor: .systemBackground, backgroundColor: .color163BDE, cornerRadius: 10, edgeInsets: .init(top: 10, leading: 80, bottom: 10, trailing: 80))
    }
    
    private func setupTransactionView(amount: Double?, currencySymbol: String?, transactionID: String?) {
        guard let amount = amount else {
            transactionSummaryView.isHidden = true
            paymentMethodListBanner.isHidden = false
            
            return
        }
        
        transactionSummaryView.isHidden = false
        paymentMethodListBanner.isHidden = true
        
        transactionBackground.backgroundColor = .colorF1F3FF
        transactionBackground.roundedCorner(radius: 14)
        
        transactionAmountBackground.backgroundColor = .colorFFEF04
        transactionAmountBackground.roundedCorner(radius: 10)
        
        transactionAmount.textColor = .color163BDE
        transactionCurrency.textColor = .color163BDE
        transactionAmount.font = .poppins(forTextStyle: .headline, type: .medium, fontSize: 40)
        transactionCurrency.font = .poppins(forTextStyle: .caption1, type: .medium, fontSize: 24)
        
        transactionAmount.text = amount.toLocal(maxFraction: 2)
        transactionCurrency.text = currencySymbol
        
        if let transactionId = transactionID {
            transactionIDLabel.isHidden = false
            transactionIDLabel.text = NSLocalizedString("Transaction ID: #", comment: "Text: Transaction ID: #") + transactionId
            transactionIDLabel.textColor = .color163BDE
            transactionIDLabel.font = .poppins(forTextStyle: .body, type: .medium, fontSize: 14)
        } else {
            transactionIDLabel.isHidden = true
        }
    }
}

extension SelectPaymnetMethodViewController {
    private func setupListView() {
        paymentList.delegate = self
        paymentList.separatorStyle = .none
        paymentList.bounces = false
        paymentList.register(cell: PaymentMethodTableViewCell.self, bundle: .module)
        
        setupDataSource()
    }
    
    private func setupDataSource() {
        dataSource = DataSource(tableView: paymentList) { tableView, indexPath, payment in
            
            guard let cell = tableView.dequeueReusableCell(cell: PaymentMethodTableViewCell.self, indexPath: indexPath) else {fatalError("Can't dequeue cell")}
            
            cell.selectionStyle = .none
            
            cell.setupCell(payment)
            
            return cell
        }
    }
    
    private func updateData(_ data: [PaymentType]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, PaymentType>()
        snapshot.appendSections([.main])
        snapshot.appendItems(data, toSection: .main)
        
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}

extension SelectPaymnetMethodViewController {
    private func setupBinding() {
        viewModel.$paymentMethods
            .receive(on: DispatchQueue.main)
            .sink { [weak self] paymentMethods in
                self?.updateData(paymentMethods)
            }
            .store(in: &cancellable)
        
        viewModel.$selectedMethod
            .receive(on: DispatchQueue.main)
            .sink { [weak self] method in
                if let method = method {
                    self?.payButton.isEnabled = true
                    
                    if method == .wallet {
                        self?.updatePayButton(title: NSLocalizedString("Make Payment", comment: "Button: Make Payment"))
                    } else {
                        self?.updatePayButton(title: NSLocalizedString("Continue to Pay", comment: "Button: Continue to Pay"))
                    }
                    
                } else {
                    self?.payButton.isEnabled = false
                }
            }
            .store(in: &cancellable)
    }
}

extension SelectPaymnetMethodViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let method = dataSource?.itemIdentifier(for: indexPath) else {return}
        
        viewModel.updateSelectedMethod(method)
    }
}

fileprivate enum Section {
    case main
}
