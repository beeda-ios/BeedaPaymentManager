// 
// Beeda Driver
//
// Created by Rakibur Khan on 30/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import UIKit
@_spi(BPM) import BeedaPaymentBase

final class PaymentCoordinator {
    var navigationController: UINavigationController?
    var delegate: (any PaymentCardDelegate)?

    init(navigationController: UINavigationController?, delegate: any PaymentCardDelegate) {
        self.navigationController = navigationController
        self.delegate = delegate
    }

    required init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func initialController(viewModel: PaymentCardListViewModel? = nil) -> SelectPaymnetMethodViewController? {
        let viewController: SelectPaymnetMethodViewController = .init(bundle: .module)
        viewController.title = NSLocalizedString("Payment Options", comment: "NavigationTitle: Payment Options")
        return viewController
    }

    func navigateToInitialView(controller: SelectPaymnetMethodViewController? = nil, animated: Bool = true) {
        var viewController = SelectPaymnetMethodViewController()

        if let controller = controller {
            viewController = controller
        } else {
            guard let controller = initialController() else {return}
            viewController = controller
        }

        navigationController?.pushViewController(viewController, animated: animated)
    }

    func navigateToCardList(controller: CardListViewController? = nil, animated: Bool = true) {
        var viewController = CardListViewController(bundle: .module)

        if let controller = controller {
            viewController = controller
        }

        viewController.title = NSLocalizedString("Payment Options", comment: "NavigationTitle: Payment Options")

        navigationController?.pushViewController(viewController, animated: animated)
    }
}

var paymentCoordinator = PaymentCoordinator(navigationController: nil)
