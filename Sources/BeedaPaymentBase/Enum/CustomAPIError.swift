// 
// Beeda Seller
//
// Created by Rakibur Khan on 19/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

@_spi(BPM) public enum CustomAPIError: Error {
    case apiDataError(error: Error, message: String)
}

@_spi(BPM) extension CustomAPIError: CustomStringConvertible {
    @_spi(BPM) public var description: String {
        switch self {
            case .apiDataError(let error, let message):
                if message.isEmpty {
                    return error.localizedDescription
                } else {
                    return message
                }
        }
    }
}

@_spi(BPM) extension CustomAPIError: LocalizedError {
    @_spi(BPM) public var errorDescription: String? {
        switch self {
            case .apiDataError(let error, let message):
                if message.isEmpty {
                    return error.localizedDescription
                } else {
                    return message
                }
        }
    }
}
