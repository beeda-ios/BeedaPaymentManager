// 
// 
//
// Created by Rakibur Khan on 9/5/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation
import RKAPIUtility

@_spi(BPM) public enum URLS {
    case addCard
    case listCard
    case deleteCard
    
    var url: URL? {
        switch self {
            case .addCard:
                return buildURL(path: "create-card")
            case .listCard:
                return buildURL(path: "card-list")
            case .deleteCard:
                return buildURL(path: "delete-card")
        }
    }
    
    private func buildURL(scheme: String = CONFIG.urlScheme, baseURL: String = CONFIG.baseURL, portNo: Int? = CONFIG.portNo, path: String, query: [URLQueryItem]? = nil) -> URL? {
        var actualPath: String = "pmt/v1/payment/card/"
        
        if let pathPrefix = CONFIG.pathPrefix {
            actualPath = pathPrefix + actualPath + path
        } else {
            actualPath = actualPath + path
        }
        
        return RKAPIHelper.buildURL(scheme: scheme, baseURL: baseURL, portNo: portNo, path: actualPath, queries: query)
    }
}
