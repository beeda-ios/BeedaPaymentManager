// 
// Beeda Driver
//
// Created by Rakibur Khan on 15/1/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import SwiftUI

@_spi(BPM) public enum PaymentType: String {
    case cash = "cash_on_delivery"
    case card = "card"
    case wallet = "wallet"
    case stripe = "stripe"
    
    @_spi(BPM) public var intValue: Int {
        var value = 0
        
        switch self {
            case .cash:
                value = 1
                
            case .card:
                value = 3
                
            case .wallet:
                value = 2
                
            case .stripe:
                value = 4
        }
        
        return value
    }
    
    @_spi(BPM) public var title: String {
        var title: String = ""
        switch self {
            case .cash:
                title = NSLocalizedString("Cash", comment: "Text: Cash")
                
            case .card:
                title = NSLocalizedString("Credit Card", comment: "Text: Credit Card")
                
            case .wallet:
                title = NSLocalizedString("Beeda Pay", comment: "Text: Beeda Pay")
                
            case .stripe:
                title = NSLocalizedString("Digital Payment", comment: "Text: Digital Payment")
        }
        
        return title
    }
    
    @_spi(BPM) public var listTitle: String {
        var title: String = ""
        switch self {
            case .cash:
                title = NSLocalizedString("Cash", comment: "Text: Cash")
                
            case .card:
                title = NSLocalizedString("Debit or credit card", comment: "Text: Debit or credit card")
                
            case .wallet:
                title = NSLocalizedString("Beeda Pay", comment: "Text: Beeda Pay")
                
            case .stripe:
                title = NSLocalizedString("Digital", comment: "Text: Digital")
        }
        
        return title
    }
    
    @_spi(BPM) public var image: Image? {
        let image: Image?
        
        switch self {
            case .cash:
                image = Image("cash_icon", bundle: .module)
                
            case .card:
                image = Image("cardImage", bundle: .module)
                
            case .wallet:
                image = Image("beeda_wallet_icon", bundle: .module)
                
            case .stripe:
                image = Image("credit_card_icon", bundle: .module)
        }
        
        return image
    }
    
    @_spi(BPM) public var icon: UIImage? {
        let image: UIImage?
        
        switch self {
            case .cash:
                image = UIImage(named: "cash_icon", in: .module, with: .none)
                
            case .card:
                image = UIImage(named: "cardImage", in: .module, with: .none)
                
            case .wallet:
                image = UIImage(named: "beeda_wallet_icon", in: .module, with: .none)
                
            case .stripe:
                image = UIImage(named: "credit_card_icon", in: .module, with: .none)
        }
        
        return image
    }
}
