//
//  CardAPIService.swift
//  Beeda Driver
//
//  Created by Rakibur Khan on 1/5/23.
//  Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

@_spi(BPM) public final class CardAPIService: CodableDataModelBase {
    private var apiService = CONFIG.networkService
//    private let keychain = CONFIG.keychain
    
    func allCards() async -> Result<[Card], Error> {
        do {
            let reply = try await apiService.fetchItems(urlLink: URLS.listCard.url)
            
            guard let rawData = reply.data else {
                debugPrint(reply.response)
                
                throw reply.response
            }
            
            guard reply.response.responseType == .success else {
                print(String(decoding: rawData, as: UTF8.self))
                debugPrint(reply.response)
                
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            let decodedOriginalData = try JSONDecoder().decode(CodableResponseRootModel<[CodableCardModel]>.self, from: rawData)
            
            if !decodedOriginalData.status {
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            guard let infoTypeData = decodedOriginalData.data else {throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)}
            
            let returnData: [Card] = infoTypeData.compactMap{.init($0)}
            
            return .success(returnData)
        } catch {
            let customError = ErrorHandllerService.shared.generateCustomError(error: error)
            return .failure(customError)
        }
    }
    
    func addNewCard(card: CardRequestModel) async -> Result<[Card], Error> {
        do {
            let dataModel = CodableAddCardRequestModel(cardNumber: card.cardNo, month: card.month, year: card.year, cvv: card.cvv, name: card.name, phone: card.phone, email: card.email, address: card.address, state: card.state, zip: card.zip, country: card.country)
            
            let reply = try await apiService.fetchItemsByHTTPMethod(urlLink: URLS.addCard.url, httpMethod: .post, body: dataModel)
            
            guard let rawData = reply.data else {
                debugPrint(reply.response)
                
                throw reply.response
            }
            
            guard reply.response.responseType == .success else {
                print(String(decoding: rawData, as: UTF8.self))
                debugPrint(reply.response)
                
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            print(String(decoding: rawData, as: UTF8.self))
            
            let decodedOriginalData = try JSONDecoder().decode(CodableResponseRootModel<[CodableCardModel]>.self, from: rawData)
            
            if !decodedOriginalData.status {
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            guard let infoTypeData = decodedOriginalData.data else {throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)}
            
            let returnData: [Card] = infoTypeData.compactMap{.init($0)}
            
            return .success(returnData)
        } catch {
            let customError = ErrorHandllerService.shared.generateCustomError(error: error)
            return .failure(customError)
        }
    }
    
    func removeCard(id: String) async -> Result<[Card], Error> {
        do {
            let dataModel = CodableRemoveCardRequestModel(id: id)
            
            let reply = try await apiService.fetchItemsByHTTPMethod(urlLink: URLS.deleteCard.url, httpMethod: .post, body: dataModel)
            
            guard let rawData = reply.data else {
                debugPrint(reply.response)
                
                throw reply.response
            }
            
            guard reply.response.responseType == .success else {
                print(String(decoding: rawData, as: UTF8.self))
                debugPrint(reply.response)
                
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            print(String(decoding: rawData, as: UTF8.self))
            
            let decodedOriginalData = try JSONDecoder().decode(CodableResponseRootModel<[CodableCardModel]>.self, from: rawData)
            
            if !decodedOriginalData.status {
                throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)
            }
            
            guard let infoTypeData = decodedOriginalData.data else {throw ErrorHandllerService.DataParsingError.parsingError(error: reply.response, data: rawData)}
            
            let returnData: [Card] = infoTypeData.compactMap{.init($0)}
            
            return .success(returnData)
        } catch {
            let customError = ErrorHandllerService.shared.generateCustomError(error: error)
            return .failure(customError)
        }
    }
}

fileprivate extension CodableDataModelBase {
    //MARK: - Card
    struct CodableCardModel: Decodable {
        let id: String
        let name: String
        let cardNo: String
        let month: Int
        let year: Int
        let type: String
        let cardImage: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case name
            case cardNo = "card_no"
            case month
            case year
            case type
            case cardImage = "card_image"
        }
    }
    
    struct CodableAddCardRequestModel: Encodable {
        let cardNumber: String
        let month: String
        let year: String
        let cvv: String
        let name: String
        let phone: String
        let email: String
        let address: String
        let state: String
        let zip: String?
        let country: String
        
        private enum CodingKeys: String, CodingKey {
            case cardNumber = "card_no"
            case month
            case year
            case cvv
            case name
            case phone = "phone_number"
            case email
            case address
            case state
            case zip
            case country
        }
    }
    
    struct CodableRemoveCardRequestModel: Encodable {
        let id: String
        
        private enum CodingKeys: String, CodingKey {
            case id = "card_id"
        }
    }
}

fileprivate extension Card {
    init(_ data: CodableDataModelBase.CodableCardModel) {
        self.init(id: data.id, name: data.name, cardNo: data.cardNo, month: data.month, year: data.year, type: data.type, icon: URL(string: data.cardImage ?? ""))
    }
}
