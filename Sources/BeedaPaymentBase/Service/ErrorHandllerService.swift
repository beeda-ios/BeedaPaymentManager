// 
// Beeda Seller
//
// Created by Rakibur Khan on 19/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation
import OSLog
import RKAPIUtility

@_spi(BPM) public class ErrorHandllerService: CodableDataModelBase {
    
    static let shared: ErrorHandllerService = ErrorHandllerService()
    
//    let logger: Logger = .init(subsystem: Preferences.loggingSubsystem, category: "hello")
    
    enum DataParsingError: Error {
        case parsingError(error: Error, data: Data)
    }
    
    private func retrieveErrorMessage(errorData: Data)-> Result<String, Error> {
        do {
            let decodedOriginalErrorMessage = try JSONDecoder().decode(CodableErrorModel.self, from: errorData)
            
            let returnData: String = decodedOriginalErrorMessage.message ?? "Unknown Reason"
            
            return .success(returnData)
        } catch {
            return .failure(error)
        }
    }
    
    func generateCustomError(error: Error) -> Error {
        guard let error: DataParsingError = error as? DataParsingError else {
            return CustomAPIError.apiDataError(error: error, message: error.localizedDescription)
        }
        
        var decodedError: Result<String, Error>
        var rawError: Error
        
        switch error {
            case .parsingError(let error, let data):
                decodedError = retrieveErrorMessage(errorData: data)
                rawError = error
        }
        
        switch decodedError {
            case .success(let message):
                return CustomAPIError.apiDataError(error: rawError, message: message)
                
            case .failure(let error):
                return CustomAPIError.apiDataError(error: error, message: error.localizedDescription)
        }
    }
    
    @discardableResult
    func checkError(_ error: Error, message: String? = nil, logout: Bool = true)-> String? {
        if let customError: CustomAPIError = error as? CustomAPIError {
            let (error, customMessage) = checkCustomError(customError)
            
            if let bug: HTTPStatusCode = error as? HTTPStatusCode {
                return  decodeHTTPStatusMessage(bug, message: customMessage, logout: logout)
            } else {
                return customMessage
            }
        }
        else {
            if let bug: HTTPStatusCode = error as? HTTPStatusCode {
                return decodeHTTPStatusMessage(bug, message: message, logout: logout)
            } else {
                return nil
            }
        }
    }
    
    private func decodeHTTPStatusMessage(_ error: HTTPStatusCode?, message: String? = nil, logout: Bool)-> String? {
        guard let bug: HTTPStatusCode = error else {
            return nil
        }
        
        switch bug.responseType {
            case .clientError:
                switch HTTPStatusCode.StandardCode(rawValue: bug.statusCode) {
                    case .unauthorized:
                        if logout {
                            Task {
                                //TODO: Need to handle logout
//                                await userViewModel.logoutCurrentUser()
                            }
                        } else {
                            break
                        }
                        
                    default:
                        break
                }
                
            default:
                return message ?? bug.localizedDescription
        }
        
        return message ?? bug.localizedDescription
    }
    
    private func checkCustomError(_ error: CustomAPIError)-> (Error, String) {
        switch error {
            case .apiDataError(let error, let message):
                return (error, message)
        }
    }
}
