//
//  PaymentConfigurationBase.swift
//  
//
//  Created by Rakibur Khan on 16/7/23.
//

import UIKit
import SwiftKeychainWrapper
import RKAPIService

@_spi(BPM) public class PaymentConfigurationBase {
    var urlScheme: String = "http"
    var baseURL: String = ""
    var pathPrefix: String?
    var portNo: Int?
    var networkService: RKAPIService = .init()
    var window: UIWindow?
    
    @_spi(BPM) public init(urlScheme: String, baseURL: String, pathPrefix: String? = nil, portNo: Int? = nil, networkService: RKAPIService, window: UIWindow?) {
        self.urlScheme = urlScheme
        self.baseURL = baseURL
        self.pathPrefix = pathPrefix
        self.portNo = portNo
        self.networkService = networkService
        self.window = window
    }
    
    init() {}
}

@_spi(BPM) public var CONFIG = PaymentConfigurationBase()
