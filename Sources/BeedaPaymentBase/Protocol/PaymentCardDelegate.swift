// 
// Beeda Driver
//
// Created by Rakibur Khan on 30/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

public protocol PaymentCardDelegate {
    func cardSelected(_ card: Card)
    func walletSelected(_ pin: String)
}
