// 
// Beeda Driver
//
// Created by Rakibur Khan on 15/12/22.
// Copyright © 2022 Beeda Inc. All rights reserved.
//

import UIKit

@_spi(BPM) public class CodableDataModelBase: NSObject {
    struct CodableResponseRootModel<T: Decodable>: Decodable {
        let status: Bool
        let message: String?
        let data: T?
        
        private enum CodingKeys: String, CodingKey {
            case status
            case message
            case data
        }
        
        init(from decoder: Decoder) throws {
            let coded = try decoder.container(keyedBy: CodingKeys.self)
            
            do {
                let tempStatus: Bool = try coded.decode(Bool.self, forKey: .status)
                
                status = tempStatus
            } catch {
                let tempStatus: Int = try coded.decode(Int.self, forKey: .status)
                
                status = tempStatus == 1
            }
            
            self.message = try coded.decodeIfPresent(String.self, forKey: .message)
            self.data = try coded.decodeIfPresent(T.self, forKey: .data)
        }
    }
    
    struct CodableErrorModel: Decodable {
        let message: String?
    }
    
    struct CodableFCMTokenUpdateModel: Encodable {
        let token: String
        var userType: String = "driver"
        
        private enum CodingKeys: String, CodingKey {
            case token = "device_token"
            case userType = "user_type"
        }
    }
    
    struct CodableMediaMedia {
        let key: String
        let filename: String
        let data: Data
        let mimeType: String
        init?(withImage image: UIImage, forKey key: String) {
            self.key = key
            self.mimeType = "image/jpeg"
            self.filename = "imagefile.jpg"
            guard let data = image.jpegData(compressionQuality: 0.7) else { return nil }
            self.data = data
        }
    }
}
