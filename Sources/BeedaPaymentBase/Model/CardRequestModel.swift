//
//  CardRequestModel.swift
//  
//
//  Created by Rakibur Khan on 14/7/23.
//

import Foundation

@_spi(BPM) public struct CardRequestModel {
    let cardNo: String
    let name: String
    let month: String
    let year: String
    let cvv: String
    let country: String
    let state: String
    let zip: String
    let phone: String
    let address: String
    let email: String
    
    @_spi(BPM) public init(cardNo: String, name: String, month: String, year: String, cvv: String, country: String, state: String, zip: String, phone: String, address: String, email: String) {
        self.cardNo = cardNo
        self.name = name
        self.month = month
        self.year = year
        self.cvv = cvv
        self.country = country
        self.state = state
        self.zip = zip
        self.phone = phone
        self.address = address
        self.email = email
    }
}
