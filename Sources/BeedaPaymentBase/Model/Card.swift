// 
// Beeda Driver
//
// Created by Rakibur Khan on 30/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

public struct Card: Hashable {
    public let id: String
    public let name: String
    public let cardNo: String
    public let month: Int
    public let year: Int
    public let type: String
    public let icon: URL?
}
