//
//  DriverSelectPaymnetMethodViewModel.swift
//  Beeda Driver
//
//  Created by Rakibur Khan on 2/5/23.
//  Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation

@_spi(BPM) public final class SelectPaymnetMethodViewModel: ObservableObject {
    @Published @_spi(BPM) public var paymentMethods: [PaymentType] = [.wallet, .card]
    
    @Published @_spi(BPM) public var selectedMethod: PaymentType?
    
    @_spi(BPM) public init(paymentMethods: [PaymentType], selectedMethod: PaymentType? = nil) {
        self.paymentMethods = paymentMethods
        self.selectedMethod = selectedMethod
    }
    
    @_spi(BPM) public init() {}
    
    @_spi(BPM) public func updateSelectedMethod(_ method: PaymentType) {
        selectedMethod = method
    }
}
