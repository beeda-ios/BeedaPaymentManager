// 
// Beeda Driver
//
// Created by Rakibur Khan on 30/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import Foundation
import OSLog

@_spi(BPM) public final class PaymentCardListViewModel: ObservableObject {
    @Published @_spi(BPM) public var cards: [Card] = []
    @Published @_spi(BPM) public var selectedCard: Card?
    @Published @_spi(BPM) public var listUpdated: Bool?
    
    private let apiService = CardAPIService()
    
    @_spi(BPM) public init(cards: [Card], selectedCard: Card? = nil, listUpdated: Bool? = nil) {
        self.cards = cards
        self.selectedCard = selectedCard
        self.listUpdated = listUpdated
    }
    
    @_spi(BPM) public init() {}
    
//    private let logger = Logger(subsystem: Preferences.loggingSubsystem, category: "card_log")
    
    @_spi(BPM) public func updateSelectedCard(_ card: Card) {
        selectedCard = card
    }
    
    @_spi(BPM) public func resetListStatus() {
        listUpdated = nil
    }
    
    @_spi(BPM) public func fetchAllCards() async {
        self.listUpdated = nil
        await MainActor.run {
            utility.showLoading()
        }
        let reply = await apiService.allCards()
        
        switch reply {
            case .success(let cards):
                await utility.hideLoading()
                self.cards.removeAll()
                self.cards.append(contentsOf: cards)
                
            case .failure(let error):
                await utility.hideLoading()
//                logger.error("\(error)")
                if let message = ErrorHandllerService.shared.checkError(error) {
                    await utility.showAlert(title: message, type: .failure)
                }
        }
    }
    
    @_spi(BPM) public func removeCard(_ id: String) {
        Task {
            await removeCard(id)
        }
    }
    
    @_spi(BPM) public func removeCard(_ id: String) async {
        self.listUpdated = nil
        await utility.showLoading()
        let reply = await apiService.removeCard(id: id)
        
        switch reply {
            case .success(let cards):
                await utility.hideLoading()
                self.cards.removeAll()
                self.cards.append(contentsOf: cards)
                
            case .failure(let error):
                await utility.hideLoading()
//                logger.error("\(error)")
                if let message = ErrorHandllerService.shared.checkError(error) {
                    await utility.showAlert(title: message, type: .failure)
                }
        }
    }
    
    @_spi(BPM) public func addCard(_ card: CardRequestModel) {
        Task {
            await addCard(card)
        }
    }
    
    @_spi(BPM) public func addCard(_ card: CardRequestModel) async {
        self.listUpdated = nil
        await utility.showLoading()
        let reply = await apiService.addNewCard(card: card)
        
        switch reply {
            case .success(let cards):
                await utility.hideLoading()
                self.cards.removeAll()
                self.cards.append(contentsOf: cards)
                self.listUpdated = true
                
            case .failure(let error):
                await utility.hideLoading()
//                logger.error("\(error)")
                if let message = ErrorHandllerService.shared.checkError(error) {
                    await utility.showAlert(title: message, type: .failure)
                }
        }
    }
}
