// 
// 
//
// Created by Rakibur Khan on 25/7/23.
// Copyright © 2023 Beeda .Inc All rights reserved.
//

import Foundation

import UIKit
import BeedaAlert

@MainActor
@_spi(BPM) public class Utility: NSObject {
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView();
    var overlayView = UIView()
    var mainView = UIView()
    var titleLabel = UILabel()
    var imgView = UIImageView()
    var alert: BeedaAlertManager = .shared
    
    private let alertViewTag: Int = 25378
    
    override init(){}
    
    @MainActor
    @_spi(BPM) public func showLoading(color: UIColor = UIColor.systemBackground) {
        if !activityIndicator.isAnimating {
            self.mainView = UIView()
            self.mainView.frame = UIScreen.main.bounds
            self.mainView.backgroundColor = .clear
            self.overlayView = UIView()
            self.imgView = UIImageView()
            self.activityIndicator = UIActivityIndicatorView()
            
            overlayView.frame = UIScreen.main.bounds///CGRect(x: 0, y: 0, width: 80, height: 80)
            overlayView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            overlayView.clipsToBounds = true
            //overlayView.layer.cornerRadius = 10
            overlayView.layer.zPosition = 1
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
            activityIndicator.style = .medium
            
            overlayView.addSubview(activityIndicator)
            self.mainView.addSubview(overlayView)
            
            if CONFIG.window?.viewWithTag(101) == nil {
                overlayView.center = CONFIG.window?.center ?? CGPoint(x: 0, y: 0)
                mainView.tag = 101
                CONFIG.window?.addSubview(mainView)
                activityIndicator.startAnimating()
            }
        }
    }
    
    @MainActor
    @_spi(BPM) public func hideLoading() {
        activityIndicator.stopAnimating()
        CONFIG.window?.viewWithTag(101)?.removeFromSuperview()
    }
    
    //MARK: Set view for hiden
    @MainActor
    @_spi(BPM) public func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve) {
            view.isHidden = hidden
        }
    }
}

//MARK: - Alert
extension Utility {
    @MainActor
    @_spi(BPM) public func showAlert(title: String, message: String? = nil, type: BeedaAlertType, defaultAction: BeedaAlertAction? = nil, cancelAction: BeedaAlertAction? = nil, outsideTapDismiss: Bool = true, customTag: Int? = nil) {
        if let window = CONFIG.window {
            alert.updateWindow(window)
            
            alert.showAlert(title: title, message: message, type: type, defaultAction: defaultAction, cancelAction: cancelAction, outsideTapDismiss: outsideTapDismiss, customTag: customTag)
        }
    }
    
    @MainActor
    @_spi(BPM) public func showAlert(icon: UIImage?, title: String, message: String? = nil, defaultAction: BeedaAlertAction? = nil, cancelAction: BeedaAlertAction? = nil, outsideTapDismiss: Bool = true, customTag: Int? = nil) {
        if let window = CONFIG.window {
            alert.updateWindow(window)
            
            alert.showAlert(icon: icon, title: title, message: message, defaultAction: defaultAction, cancelAction: cancelAction, outsideTapDismiss: outsideTapDismiss, customTag: customTag)
        }
    }
    
    @MainActor
    @_spi(BPM) public func removeAlert(customTag: Int? = nil) {
        alert.removeAlert(customTag: customTag)
    }
}

@MainActor
@_spi(BPM) public var utility = Utility()
