// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BeedaPaymentManager",
    platforms: [.iOS(.v14)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "BeedaPaymentManager",
            targets: ["BeedaPaymentManager"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/TheRakiburKhan/RKAPIService.git", from: Version(3, 0, 0)),
        .package(url: "https://github.com/jrendel/SwiftKeychainWrapper.git", from: Version(4, 0, 0)),
        .package(url: "https://github.com/SURYAKANTSHARMA/CountryPicker.git", from: Version(2, 0, 0)),
        .package(url: "https://github.com/stripe/stripe-ios.git", from: Version(23, 0, 0)),
        .package(url: "https://lab.easital.com/beeda-platform/beeda-ios/BeedaUtilities.git", from: Version(1, 0, 0)),
        .package(url: "https://lab.easital.com/beeda-platform/beeda-ios/BeedaAlert.git", from: Version(1, 0, 0))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "BeedaPaymentManager",
            dependencies: ["CountryPicker", .product(name: "Stripe", package: "stripe-ios"), "BeedaUtilities", "BeedaPaymentBase", "BeedaAlert"]
        ),
        .target(
            name: "BeedaPaymentBase",
            dependencies: ["RKAPIService", "SwiftKeychainWrapper", "BeedaUtilities", "BeedaAlert"]
        ),
        .testTarget(
            name: "BeedaPaymentManagerTests",
            dependencies: ["BeedaPaymentManager"]),
    ]
)
